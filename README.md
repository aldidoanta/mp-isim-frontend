# mp-isim-frontend

The Master Project of Aldi Doanta Kurnia - Master Computer Science student at the University of Twente.

This is the Mendix frontend of the [`mp-isim`](https://github.com/aldidoanta/mp-isim) repo.
The repo is automatically updated whenever commits are pushed from the Mendix Studio Pro desktop app.
